package com.sysomos.automation.InterviewTest;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BaseTest {
	
	protected WebDriver driver;
	
	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
		this.driver = new ChromeDriver();
//		this.driver = new FirefoxDriver();
		driver.get("http://nibbler.silktide.com/");
	}

	@After
	public void teardown() {
		driver.quit();
	}
}
