package com.sysomos.automation.InterviewTest;

import org.junit.Assert;
import org.junit.Test;

public class TestWebsite extends BaseTest
{
   @Test
   public void test1() {
	   String input = "www.google.com";
	   MainPage mainpage = new MainPage(driver);
	   mainpage.enterAddress(input);
	   mainpage.clickTestButton();
	   
	   ReportPage reportPage = new ReportPage(driver);
	   Assert.assertEquals(input, reportPage.getReportHeaderUrl());
	   System.out.println(reportPage.getOverallScore());
   }
   
	@Test
	public void test2() {
		String input = "badurl";
		MainPage mainpage = new MainPage(driver);
		mainpage.enterAddress(input);
		mainpage.clickTestButton();

		Assert.assertEquals(
				"The website \"http://badurl\" is currently unavailable. Please check the address you entered or try again later.",
				mainpage.getErrorMessage());
	}
}
