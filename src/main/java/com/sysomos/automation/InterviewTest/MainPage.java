package com.sysomos.automation.InterviewTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends BasePage
{
	@FindBy (id = "url")
	private WebElement addressTextField;
	
	@FindBy (css = "input[value='Test']")
	private WebElement testButton;
	
	@FindBy (css = ".errors li")
	private WebElement errorMessage;
	
    public MainPage(WebDriver driver) {
    	super(driver);
    }
    
    public void enterAddress(String address) {
    	addressTextField.sendKeys(address);
    }
    
    public void clickTestButton() {
    	testButton.click();
    }
    
    public String getErrorMessage() {
    	return errorMessage.getText().trim();
    }
}
