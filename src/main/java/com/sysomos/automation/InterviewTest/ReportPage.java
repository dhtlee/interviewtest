package com.sysomos.automation.InterviewTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ReportPage extends BasePage {
	
	@FindBy (css = "#report-header>h1>a")
	private WebElement reportHeaderUrl;
	
	@FindBy (css = "#summaries>div:nth-of-type(1) span")
	private WebElement overallScore;

	public ReportPage(WebDriver driver) {
		super(driver);
	}
	
	public String getReportHeaderUrl() {
		return reportHeaderUrl.getText();
	}
	
	public String getOverallScore() {
		return overallScore.getText();
	}
}
